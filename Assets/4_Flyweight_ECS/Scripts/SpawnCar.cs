using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCar : MonoBehaviour {

    [field: SerializeField] public GameObject car { get; private set; }
    [field: SerializeField] public GameObject mainCamera { get; private set; }

    private SmoothFollow camTarget;


    // Start is called before the first frame update
    void Start() {
        camTarget = mainCamera.GetComponent<SmoothFollow>();

        //StartCoroutine(StartCo());
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(KeyCode.Space)) {
            Vector3 pos = new Vector3(10, 10, 10);
            GameObject c = Instantiate(car, pos, Quaternion.identity);
            camTarget.target = c.transform;
        }
    }

    private IEnumerator StartCo() {

        camTarget = mainCamera.GetComponent<SmoothFollow>();
        
        yield return null;
    }
}
