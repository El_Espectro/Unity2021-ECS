using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCubes : MonoBehaviour {

    [field: SerializeField] public GameObject cube { get; private set; }
    [field: SerializeField] public int rows { get; private set; }
    [field: SerializeField] public int cols { get; private set; }


    // Start is called before the first frame update
    void Start() {
        StartCoroutine(StartCo());
    }

    // Update is called once per frame
    void Update() {
        
    }

    private IEnumerator StartCo() {
        for (int x = 0; x < rows; x++) {
            for (int z = 0; z < cols; z++) {
                GameObject instance = Instantiate(cube);
                Vector3 pos = new Vector3 (x,
                    Mathf.PerlinNoise(x * 0.21f, z * 0.21f),
                    z);

                instance.transform.position = pos;
            }
        }
        yield return null;
    }
}
