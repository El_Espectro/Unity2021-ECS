using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Entities.CodeGeneratedJobForEach;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Rendering;

public partial class SpawnerSystem : SystemBase {
    EndSimulationEntityCommandBufferSystem m_EntityCommandBufferSystem;

    protected override void OnCreate() {
        m_EntityCommandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }


    /*struct SpawnJob : IJobForEachWithEntity<Spawner, LocalToWorld> {
        public EntityCommandBuffer CommandBuffer;
        public void Execute(Entity entity, int index, [ReadOnly] ref Spawner spawner, [ReadOnly] ref LocalToWorld location) {
            for (int x = 0; x < spawner.Erows; x++) {
                for (int z = 0; z < spawner.Ecols; z++) {
                    var instance = CommandBuffer.Instantiate(spawner.Prefab);
                    var pos = math.transform(location.Value, new float3(x, noise.cnoise(new float2(x, z) * 0.021f), z));
                    CommandBuffer.SetComponent(instance, new Translation { Value = pos });
                }
            }
            CommandBuffer.DestroyEntity(entity);
        }
    }*/

    /*protected override JobHandle OnUpdate(JobHandle inputDeps) {
        var job = new SpawnJob {
            CommandBuffer = m_EntityCommandBufferSystem.CreateCommandBuffer()
        }.ScheduleSingle(this, inputDeps);

        m_EntityCommandBufferSystem.AddJobHandleForProducer(job);
        return job;
    }*/

    /*struct SpawnJob  {
        public EntityCommandBuffer commandBuffer;

        Entities.WithAll<Spawner, LocalToWorld>().Fo

        public void Execute(Entity entity, int index, in Spawner spawner, in LocalToWorld location) {
            for (int x = 0; x < spawner.Erows; x++) {
                for (int z = 0; z < spawner.Ecols; z++) {
                    var instance = commandBuffer.Instantiate(spawner.Prefab);
                    var pos = math.transform(location.Value, new float3(x, noise.cnoise(new float2(x, z) * 0.021f), z));
                    commandBuffer.SetComponent(instance, new Translation { Value = pos });
                }
            }
            commandBuffer.DestroyEntity(entity);
        }
    }*/

    partial struct SpawnJob : IJobEntity {
        public EntityCommandBuffer CommandBuffer;
        
        public void Execute(Entity entity, [EntityInQueryIndex] int index, in Spawner spawner, in LocalToWorld location) {
            for (int x = 0; x < spawner.Erows; x++) {
                for (int z = 0; z < spawner.Ecols; z++) {
                    var instance = CommandBuffer.Instantiate(spawner.Prefab);
                    var pos = math.transform(location.Value, new float3(x, noise.cnoise(new float2(x, z) * 0.021f), z));
                    CommandBuffer.SetComponent(instance, new Translation { Value = pos });
                }
            }
            CommandBuffer.DestroyEntity(entity);
        }

    }

    protected override void OnUpdate() {

        var job = new SpawnJob {
            CommandBuffer = m_EntityCommandBufferSystem.CreateCommandBuffer()
        };
        job.Schedule();

        #region Default Copilot
        /*var commandBuffer = m_EntityCommandBufferSystem.CreateCommandBuffer();

        Entities
            .WithAll<Spawner, LocalToWorld>()
            .ForEach((Entity entity, int entityInQueryIndex, in Spawner spawner, in LocalToWorld location) => {
                for (int x = 0; x < spawner.Erows; x++) {
                    for (int z = 0; z < spawner.Ecols; z++) {
                        var instance = commandBuffer.Instantiate(spawner.Prefab);
                        var pos = math.transform(location.Value, new float3(x, noise.cnoise(new float2(x, z) * 0.021f), z));
                        commandBuffer.SetComponent(instance, new Translation { Value = pos });
                    }
                }
                commandBuffer.DestroyEntity(entity);
            }).Schedule();

        m_EntityCommandBufferSystem.AddJobHandleForProducer(Dependency);*/
        #endregion
    }
}

/*struct RotationSystem : SystemBase {
    protected override void OnUpdate() {
        var job = new SpawerSystem.SpawnJob {
            CommandBuffer = m_EntityCommandBufferSystem.CreateCommandBuffer()
        };
        job.Schedule();
        // Note: Schedule has to be called 
        // from variable for sourcegen to work
    }
}*/
